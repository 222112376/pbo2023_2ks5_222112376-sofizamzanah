package com.sofiznh.ProjectAkhirPBO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectAkhirPboApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectAkhirPboApplication.class, args);
	}

}
