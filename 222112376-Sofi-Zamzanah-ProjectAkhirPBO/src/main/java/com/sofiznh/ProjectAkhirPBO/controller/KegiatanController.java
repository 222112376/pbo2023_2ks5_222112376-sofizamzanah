/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.controller;

import com.sofiznh.ProjectAkhirPBO.dto.KegiatanDto;
import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;
import com.sofiznh.ProjectAkhirPBO.repository.KegiatanRepository;
import com.sofiznh.ProjectAkhirPBO.service.KegiatanService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ASUS
 */
@Controller
public class KegiatanController {
    private KegiatanService kegiatanService;
    public KegiatanController(KegiatanService kegiatanService){
        this.kegiatanService = kegiatanService;
    }
    
    @Autowired
    private KegiatanRepository kegiatanRepo;
    
    @GetMapping("/admin/kegiatans")
    public String kegiatans(Model model, @RequestParam(defaultValue="") String keyword){
        model.addAttribute("keyword", keyword);
        List<KegiatanDto> kegiatanDtos;
        if (keyword.isEmpty()){
            kegiatanDtos = this.kegiatanService.ambilDaftarKegiatan();
        } else{
            kegiatanDtos = this.kegiatanService.searchKegiatan(keyword);
        }
        
        //tambah atribut "students" yang bisa/akan digunakan di view
        model.addAttribute("kegiatanDtos", kegiatanDtos);
        if(kegiatanDtos.isEmpty()) {
            model.addAttribute("notFound", true);
        }
        // thymeleaf view: "/templates/admin/student.html"
        return "/admin/kegiatans";
    }
    
    @GetMapping("/admin/kegiatans/add")
    public String addKegiatanForm(Model model){
        KegiatanDto kegiatanDto = new KegiatanDto();
        //List<Kegiatan> listKegiatans = kegiatanRepo.findAll();
        //model.addAttribute("listKegiatans", listKegiatans);
        model.addAttribute("kegiatanDto",kegiatanDto);;
        return "/admin/kegiatan_add_form";
    }
    
    @PostMapping("/admin/kegiatans/add")
    public String addKegiatan(@Valid @ModelAttribute("kegiatanDto") KegiatanDto kegiatanDto, BindingResult result) {
    if (result.hasErrors()) {
        return "/admin/kegiatan_add_form";
    }
    
    // Hitung rata-rata dari nilai1 dan nilai2
    double rataRata = (kegiatanDto.getPenilaianWaktu() + kegiatanDto.getPenilaianBiaya() + kegiatanDto.getPenilaianKuantitas()) / 3;
    kegiatanDto.setNilaiTotal(rataRata);
    
    kegiatanService.simpanDataKegiatan(kegiatanDto);
    return "redirect:/admin/kegiatans";
    }

        
    @GetMapping("/admin/kegiatans/delete/{id}")
    public String deleteKegiatan(@PathVariable("id") Long id){
        kegiatanService.hapusDataKegiatan(id);
        return "redirect:/admin/kegiatans";
    }
    
    @GetMapping("/admin/kegiatans/update/{id}")
    public String updateKegiatanForm(@PathVariable("id") Long id, Model model){
        KegiatanDto kegiatanDto = kegiatanService.cariById(id);
        //List<Kegiatan> listKegiatans = kegiatanRepo.findAll();
        //model.addAttribute("listKegiatans", listKegiatans);
        model.addAttribute("kegiatanDto", kegiatanDto);
        return "/admin/kegiatan_update_form";
    }
    
    @PostMapping("/admin/kegiatans/update")
    public String updatekegiatan(@Valid @ModelAttribute("kegiatanDto") KegiatanDto kegiatanDto, BindingResult result){
        if (result.hasErrors()){
            return "/admin/kegiatan_update_form";
        }
        double rataRata = (kegiatanDto.getPenilaianWaktu() + kegiatanDto.getPenilaianBiaya() + kegiatanDto.getPenilaianKuantitas()) / 3;
    kegiatanDto.setNilaiTotal(rataRata);
        kegiatanService.perbaruiDataKegiatan(kegiatanDto);
        return "redirect:/admin/kegiatans";
    }
    
    
}
