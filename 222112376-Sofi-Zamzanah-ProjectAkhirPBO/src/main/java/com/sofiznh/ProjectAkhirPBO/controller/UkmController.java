/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.controller;

import com.sofiznh.ProjectAkhirPBO.dto.UkmDto;
import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;
import com.sofiznh.ProjectAkhirPBO.repository.KegiatanRepository;
import com.sofiznh.ProjectAkhirPBO.service.UkmService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ASUS
 */
@Controller
public class UkmController {
    private UkmService ukmService;
    public UkmController(UkmService ukmService){
        this.ukmService = ukmService;
    }
    
    @Autowired
    private KegiatanRepository kegiatanRepo;
    
    @GetMapping("/admin/ukms")
    public String ukms(Model model, @RequestParam(defaultValue="") String keyword){
        model.addAttribute("keyword", keyword);
        List<UkmDto> ukmDtos;
        if (keyword.isEmpty()){
            ukmDtos = this.ukmService.ambilDaftarUkm();
        } else{
            ukmDtos = this.ukmService.searchUkm(keyword);
        }
        
        model.addAttribute("ukmDtos",ukmDtos);
        if(ukmDtos.isEmpty()) {
            model.addAttribute("notFound", true);
        }
        return "/admin/ukms";
    }
    
    @GetMapping("/")
    public String index(){
        return "index";
    }
    
    @GetMapping("/admin/ukms/add")
    public String addUkmForm(Model model){
        UkmDto ukmDto = new UkmDto();
        List<Kegiatan> listKegiatans = kegiatanRepo.findAll();
        model.addAttribute("listKegiatans", listKegiatans);
        model.addAttribute("ukmDto",ukmDto);
        return "/admin/ukm_add_form";
    }
    
    @PostMapping("/admin/ukms/add")
    public String addUkm(@Valid UkmDto ukmDto, BindingResult result,Model model){
        if (result.hasErrors()){
            List<Kegiatan> listKegiatans = kegiatanRepo.findAll();
            model.addAttribute("listKegiatans", listKegiatans);
            return "/admin/ukm_add_form";
        }
        ukmService.simpanDataUkm(ukmDto);
        return "redirect:/admin/ukms";
    }
        
    @GetMapping("/admin/ukms/delete/{id}")
    public String deleteUkm(@PathVariable("id") Long id){
        ukmService.hapusDataUkm(id);
        return "redirect:/admin/ukms";
    }
    
    @GetMapping("/admin/ukms/update/{id}")
    public String updateUkmForm(@PathVariable("id") Long id, Model model){
        UkmDto ukmDto = ukmService.cariById(id);
        List<Kegiatan> listKegiatans = kegiatanRepo.findAll();
        model.addAttribute("listKegiatans", listKegiatans);
        model.addAttribute("ukmDto", ukmDto);
        return "/admin/ukm_update_form";
    }
    
    @PostMapping("/admin/ukms/update")
    public String updateUkm(@Valid @ModelAttribute("ukmDto") UkmDto ukmDto, BindingResult result){
        if (result.hasErrors()){
            return "/admin/ukm_update_form";
        }
        ukmService.perbaruiDataUkm(ukmDto);
        return "redirect:/admin/ukms";
    }
}
