/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.dto;

import com.sofiznh.ProjectAkhirPBO.entity.Ukm;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ASUS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KegiatanDto {
    private Long id;
    
    @NotEmpty (message="Activity Name should not be empty")
    private String namaKegiatan;
    

    @ManyToOne(fetch = FetchType.LAZY)
    private Ukm ukm;
    
    @NotNull(message = "Grade Point should not be empty")
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Integer penilaianWaktu;

    @NotNull(message = "Grade Point should not be empty")
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Integer penilaianBiaya;
    
    @NotNull(message = "Grade Point should not be empty")
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Integer penilaianKuantitas;
    
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Double nilaiTotal;
}
