/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.dto;

import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ASUS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UkmDto {
    private Long id;
    
    @NotEmpty (message="Name should not be empty")
    private String namaUkm;
    
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @JoinColumn(name= "ukm_id")
    private List<Kegiatan> kegiatan = new ArrayList<>();
    
    @NotNull(message = "Grade Point should not be empty")
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Integer nilai;
}
