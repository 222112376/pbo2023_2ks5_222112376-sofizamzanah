/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.mapper;

import com.sofiznh.ProjectAkhirPBO.dto.KegiatanDto;
import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;

/**
 *
 * @author ASUS
 */
public class KegiatanMapper {
    public static KegiatanDto mapToKegiatanDto (Kegiatan kegiatan) {
        //membuat dto dengan builder pattern (inject dari lombok)
        KegiatanDto kegiatanDto = KegiatanDto.builder()
            .id(kegiatan.getId())
            .namaKegiatan (kegiatan.getNamaKegiatan() )
            .ukm(kegiatan.getUkm())
            .penilaianWaktu(kegiatan.getPenilaianWaktu())
            .penilaianBiaya(kegiatan.getPenilaianBiaya())
            .penilaianKuantitas(kegiatan.getPenilaianKuantitas())
            .nilaiTotal(kegiatan.getNilaiTotal())
            .build();
    return kegiatanDto;
    }
    public static Kegiatan mapToKegiatan (KegiatanDto kegiatanDto) {
    Kegiatan kegiatan = Kegiatan.builder()
        .id (kegiatanDto.getId())
        .namaKegiatan (kegiatanDto.getNamaKegiatan() )
        .ukm (kegiatanDto.getUkm() )
        .penilaianWaktu(kegiatanDto.getPenilaianWaktu())
        .penilaianBiaya(kegiatanDto.getPenilaianBiaya())
        .penilaianKuantitas(kegiatanDto.getPenilaianKuantitas())
        .nilaiTotal(kegiatanDto.getNilaiTotal())
        .build();
    return kegiatan;
    }
}
