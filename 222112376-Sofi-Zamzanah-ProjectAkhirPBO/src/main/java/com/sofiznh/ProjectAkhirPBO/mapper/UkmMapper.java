/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.mapper;

import com.sofiznh.ProjectAkhirPBO.dto.UkmDto;
import com.sofiznh.ProjectAkhirPBO.entity.Ukm;

/**
 *
 * @author ASUS
 */
public class UkmMapper {
    public static UkmDto mapToUkmDto (Ukm ukm) {
        //membuat dto dengan builder pattern (inject dari lombok)
        UkmDto ukmDto = UkmDto.builder()
            .id(ukm.getId())
            .namaUkm (ukm.getNamaUkm() )
            .kegiatan(ukm.getKegiatan())
            .nilai(ukm.getNilai())
//            .keterangan(ukm.getKeterangan())
            .build();
    return ukmDto;
    }
    public static Ukm mapToUkm (UkmDto ukmDto) {
    Ukm ukm = Ukm.builder()
        .id (ukmDto.getId())
        .namaUkm (ukmDto.getNamaUkm() )
        .kegiatan (ukmDto.getKegiatan() )
        .nilai(ukmDto.getNilai())
//        .keterangan(ukmDto.getKeterangan())
        .build();
    return ukm;
    }
}
