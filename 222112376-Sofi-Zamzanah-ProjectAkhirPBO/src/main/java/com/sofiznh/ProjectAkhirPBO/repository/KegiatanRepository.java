/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.repository;

import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASUS
 */
@Repository
public interface KegiatanRepository extends JpaRepository<Kegiatan, Long>{
    List<Kegiatan> findByNamaKegiatanContaining (String keyword);
}
