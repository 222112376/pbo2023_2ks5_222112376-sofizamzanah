/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.sofiznh.ProjectAkhirPBO.entity.Role;
/**
 *
 * @author asus
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
    Role findByName(String name);
}
