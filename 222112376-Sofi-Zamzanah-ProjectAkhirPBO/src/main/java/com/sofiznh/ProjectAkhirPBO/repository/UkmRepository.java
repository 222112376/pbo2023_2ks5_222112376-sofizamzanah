/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.repository;

import com.sofiznh.ProjectAkhirPBO.entity.Ukm;
import java.util.List;
//import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASUS
 */
@Repository
public interface UkmRepository extends JpaRepository<Ukm, Long>{
    //Optional<Student> findByLastName(String lastName);
    List<Ukm> findByNamaUkmContaining (String keyword);
    //List<Ukm> findByKeteranganContaining (String keyword);
}
