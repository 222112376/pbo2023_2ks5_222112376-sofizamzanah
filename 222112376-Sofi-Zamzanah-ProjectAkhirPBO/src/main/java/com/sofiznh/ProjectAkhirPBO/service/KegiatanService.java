/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.service;

import com.sofiznh.ProjectAkhirPBO.dto.KegiatanDto;
import java.util.List;

/**
 *
 * @author ASUS
 */

public interface KegiatanService {
    public List<KegiatanDto> ambilDaftarKegiatan();
    public void perbaruiDataKegiatan(KegiatanDto kegiatanDto);
    public void hapusDataKegiatan(Long kegiatanId);
    public void simpanDataKegiatan(KegiatanDto kegiatanDto);
   
    public KegiatanDto cariById(Long id);
    
    public List<KegiatanDto> searchKegiatan (String keyword);
}

