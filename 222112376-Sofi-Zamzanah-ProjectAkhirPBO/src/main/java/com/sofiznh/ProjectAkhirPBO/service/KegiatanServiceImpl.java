/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.service;

import com.sofiznh.ProjectAkhirPBO.dto.KegiatanDto;
import com.sofiznh.ProjectAkhirPBO.entity.Kegiatan;
import com.sofiznh.ProjectAkhirPBO.mapper.KegiatanMapper;
import com.sofiznh.ProjectAkhirPBO.repository.KegiatanRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS
 */
@Service
public class KegiatanServiceImpl implements KegiatanService{
    private KegiatanRepository kegiatanRepository;
    
    public KegiatanServiceImpl(KegiatanRepository kegiatanRepository){
        this.kegiatanRepository = kegiatanRepository;
    }
    
    @Override
    public List<KegiatanDto> ambilDaftarKegiatan(){
        List<Kegiatan> kegiatans = this.kegiatanRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi MAP pada Array
        List<KegiatanDto> kegiatanDtos = kegiatans.stream()
                .map((kegiatan) -> KegiatanMapper.mapToKegiatanDto(kegiatan))
                .collect(Collectors.toList());
        return kegiatanDtos;
    }
      
    @Override
    public void hapusDataKegiatan(Long kegiatanId){
        this.kegiatanRepository.deleteById(kegiatanId);
    }
    
    @Override
    public void perbaruiDataKegiatan( KegiatanDto kegiatanDto ){
        Kegiatan kegiatan = KegiatanMapper.mapToKegiatan(kegiatanDto);
        this.kegiatanRepository.save(kegiatan);
    }
    
    @Override
    public void simpanDataKegiatan(KegiatanDto kegiatanDto){
        Kegiatan kegiatan = KegiatanMapper.mapToKegiatan(kegiatanDto);
        kegiatanRepository.save(kegiatan);
    }
      
    @Override
    public KegiatanDto cariById(Long id){
        Kegiatan kegiatan = kegiatanRepository.findById(id).orElse(null);
        KegiatanDto kegiatanDto = KegiatanMapper.mapToKegiatanDto(kegiatan);
        return kegiatanDto;
    }

    @Override
    public List<KegiatanDto> searchKegiatan (String keyword) {
        List<Kegiatan> kegiatans = this.kegiatanRepository
        .findByNamaKegiatanContaining(keyword);
        List<KegiatanDto> kegiatanDto = kegiatans.stream()
            .map((kegiatan) -> (KegiatanMapper.mapToKegiatanDto(kegiatan)))
            .collect(Collectors.toList());
        return kegiatanDto;
    }

}
