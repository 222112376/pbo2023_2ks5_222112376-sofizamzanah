/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.service;

import com.sofiznh.ProjectAkhirPBO.dto.UkmDto;
import java.util.List;

/**
 *
 * @author ASUS
 */

public interface UkmService {
    public List<UkmDto> ambilDaftarUkm();
    public void perbaruiDataUkm(UkmDto ukmDto);
    public void hapusDataUkm(Long ukmId);
    public void simpanDataUkm(UkmDto ukmDto);
    public UkmDto cariById(Long id);
    
    public List<UkmDto> searchUkm (String keyword);
}
