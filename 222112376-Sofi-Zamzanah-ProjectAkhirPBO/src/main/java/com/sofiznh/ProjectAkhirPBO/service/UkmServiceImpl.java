/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.service;

import com.sofiznh.ProjectAkhirPBO.dto.UkmDto;
import com.sofiznh.ProjectAkhirPBO.entity.Ukm;
import com.sofiznh.ProjectAkhirPBO.mapper.UkmMapper;
import com.sofiznh.ProjectAkhirPBO.repository.UkmRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS
 */
@Service
public class UkmServiceImpl implements UkmService{
    private UkmRepository ukmRepository;
    
    public UkmServiceImpl(UkmRepository ukmRepository){
        this.ukmRepository = ukmRepository;
    }
    
    @Override
    public List<UkmDto> ambilDaftarUkm(){
        List<Ukm> ukms = this.ukmRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi MAP pada Array
        List<UkmDto> ukmDtos = ukms.stream()
                .map((ukm) -> UkmMapper.mapToUkmDto(ukm))
                .collect(Collectors.toList());
        return ukmDtos;
    }
      
    @Override
    public void hapusDataUkm(Long ukmId){
        this.ukmRepository.deleteById(ukmId);
    }
    
    @Override
    public void perbaruiDataUkm( UkmDto ukmDto ){
        Ukm ukm = UkmMapper.mapToUkm(ukmDto);
        this.ukmRepository.save(ukm);
    }
    
    @Override
    public void simpanDataUkm(UkmDto ukmDto){
        Ukm ukm = UkmMapper.mapToUkm(ukmDto);
        ukmRepository.save(ukm);
    }
    @Override
    public UkmDto cariById(Long id){
        Ukm ukm = ukmRepository.findById(id).orElse(null);
        UkmDto ukmDto = UkmMapper.mapToUkmDto(ukm);
        return ukmDto;
    }
    
    public List<UkmDto> searchUkm (String keyword) {
        List<Ukm> ukms = this.ukmRepository
        .findByNamaUkmContaining(keyword);
        List<UkmDto> ukmDto = ukms.stream()
            .map((ukm) -> (UkmMapper.mapToUkmDto(ukm)))
            .collect(Collectors.toList());
        return ukmDto;
    }

}
