/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sofiznh.ProjectAkhirPBO.service;
import java.util.List;
import com.sofiznh.ProjectAkhirPBO.dto.UserDto;
import com.sofiznh.ProjectAkhirPBO.entity.User;

/**
 *
 * @author asus
 */
public interface UserService {
    void saveUser(UserDto userDto);

    User findUserByEmail(String email);

    List<UserDto> findAllUsers();
}
