package com.sofizamzanah.studentdemocrudwebapp.controler;

import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;
import com.sofizamzanah.studentdemocrudwebapp.mapper.StudentMapper;
import com.sofizamzanah.studentdemocrudwebapp.repository.StudentRepository;
import com.sofizamzanah.studentdemocrudwebapp.service.StudentService;
import com.sofizamzanah.studentdemocrudwebapp.service.StudentServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
/**
 *
 * @author ASUS
 */
@Controller

public class StudentController {
    private StudentService studentService;
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }
    // handler method untuk request view index
    @GetMapping("/")
    public String index(){
        return "index";
    }
    @GetMapping("/admin/students")
    public String findStudent(Model model, @RequestParam(name = "firstName", defaultValue = "") String firstName, @RequestParam(name = "lastName", defaultValue = "") String lastName,
                              @RequestParam(name = "gradePoint", defaultValue = "") Integer gradePoint, @RequestParam(name = "startDate", defaultValue = "") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
                              @RequestParam(name = "endDate", defaultValue = "") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int size){
        List<Student> students;
        List<StudentDto> studentDtos;
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Student> studentPage = null;
        if (firstName.isBlank() && lastName.isBlank() && gradePoint == null && startDate == null && endDate == null) {
            studentPage = studentService.findAll(pageable);
        } else {
            if (!firstName.isBlank()){
                studentPage = studentService.findByFirstNameContainingIgnoreCase(firstName, pageable);
                model.addAttribute("keyword", firstName);
            }
            if (!lastName.isBlank()){
                System.out.println("hehe");
                studentPage = studentService.findByLastNameContainingIgnoreCase(lastName, pageable);
                model.addAttribute("keyword", lastName);
            }
            if (gradePoint != null){
                studentPage = studentService.findByGradePointLessThan(gradePoint, pageable);
                model.addAttribute("keyword", gradePoint);
            }
            if (startDate != null && endDate != null){
                studentPage = studentService.findByBirthDateAfterAndBirthDateBefore(startDate, endDate, pageable);
                model.addAttribute("keyword", startDate);
            }
            if (!firstName.isBlank() && !lastName.isBlank()){
                System.out.println("BERHASIL");
                studentPage = studentService.findByFirstNameIsAndLastNameIs(firstName, lastName, pageable);
                model.addAttribute("keyword", firstName);
            }
        }
        // Mendapatkan daftar siswa dari halaman hasil pencarian
        students = studentPage.getContent();
        // Mengubah daftar siswa menjadi daftar StudentDto menggunakan metode mapping dari StudentMapper
        studentDtos = students.stream()
                .map(student -> StudentMapper.mapToStudentDto(student))
                .collect(Collectors.toList());
        model.addAttribute("studentDtos", studentDtos);
        model.addAttribute("currentPage", studentPage.getNumber() + 1);
        model.addAttribute("totalItems", studentPage.getTotalElements());
        model.addAttribute("totalPages", studentPage.getTotalPages());
        model.addAttribute("pageSize", size);
        return "/admin/students";
    }
    @GetMapping("/admin/students/add")
    public String addStudentForm(Model model){
        StudentDto studentDto = new StudentDto();
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object
        model.addAttribute("studentDto",studentDto);
        // thymeleaf view: "/template/admin/students.html"
        return "/admin/student_add_form";
    }
    @PostMapping("/admin/students/add")
    public String addStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_add_form";
        }
        // panggil service untuk menyimpan data student baru ke dalam database
        studentService.simpanDataStudent(studentDto);
        // langsung menuju ke halaman URL "/admin/students"
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/delete/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        // panggil service untuk menghapus data student dari dalam database
        studentService.hapusDataStudent(id);
        // langsung menuju ke halaman URL "/admin/students"
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/update/{id}")
    public String updateStudentForm(@PathVariable("id") Long id, Model model){
        // panggil service untuk mencari suatu data student berdasarkan nilai id, kemudian disimpan di dalam variabel
        StudentDto studentDto = studentService.cariById(id);
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object, yang nilainya kita cari sebelumnya
        model.addAttribute("studentDto", studentDto);
        // langsung menuju ke halaman URL "/admin/student_update_form"
        return "/admin/student_update_form";
    }

    @PostMapping("/admin/students/update")
    public String updateStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result){
        // apabila result form terdapat validasi error
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_update_form";
        }
        // panggil service untuk memperbarui suatu data student di dalam database
        studentService.perbaruiDataStudent(studentDto);
        // langsung menuju ke halaman URL "/admin/student"
        return "redirect:/admin/students";
    }
}

/*
* Penjelasan Tiap-Tiap Method
* 1. students : Method yang digunakan untuk menampilkan daftar student ke dalam tampilan halaman web. Method ini memiliki anotasi @GetMapping
*    dengan parameter /admin/students yang menandakan bahwa method ini akan menangani permintaan GET pada URL /admin/students.
* 2. index : Method yang digunakan untuk menampilkan halaman index. Method ini memiliki anotasi @GetMapping dengan parameter / yang menandakan
*    bahwa method ini akan menangani permintaan GET pada URL /.
* 3. addStudentForm : Method yang digunakan untuk menampilkan form tambah student ke dalam tampilan halaman web. Method ini memiliki anotasi
*    @GetMapping dengan parameter /admin/students/add yang menandakan bahwa method ini akan menangani permintaan GET pada URL /admin/students/add.
* 4. addStudent : Method yang digunakan untuk menambahkan data student ke dalam database. Method ini memiliki anotasi @PostMapping dengan parameter
*    /admin/students/add yang menandakan bahwa method ini akan menangani permintaan POST pada URL /admin/students/add.
* 5. deleteStudent : Method yang digunakan untuk menghapus data student dari database berdasarkan id. Method ini memiliki anotasi @GetMapping dengan
*    parameter /admin/students/delete/{id} yang menandakan bahwa method ini akan menangani permintaan GET pada URL /admin/students/delete/{id}.
* 6. updateStudentForm : Method yang digunakan untuk menampilkan form update student ke dalam tampilan halaman web. Method ini memiliki anotasi
*    @GetMapping dengan parameter /admin/students/update/{id} yang menandakan bahwa method ini akan menangani permintaan GET pada URL /admin/students/update/{id}.
* 7. updateStudent : Method yang digunakan untuk memperbarui data student ke dalam database. Method ini memiliki anotasi @PostMapping dengan parameter
*    /admin/students/update yang menandakan bahwa method ini akan menangani permintaan POST pada URL /admin/students/update
*/