package com.sofizamzanah.studentdemocrudwebapp.dto;

/**
 *
 * @author ASUS
 */

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder; //Biulder Pattern
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    private Long id;
    
    @NotEmpty(message = "First Name should not be empty")
    private String firstName;
    
    @NotEmpty(message = "Last Name should not be empty")
    private String lastName;
    
    @NotNull(message = "Grade Point should not be empty")
    @Min(value = 0, message = "Grade point should greater than 0")
    @Max(value = 100, message = "Grade point should lower than 100")
    private Integer gradePoint;
    
    @NotNull (message = "Birth Date should not be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn; 
}
