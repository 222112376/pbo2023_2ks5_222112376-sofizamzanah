package com.sofizamzanah.studentdemocrudwebapp.mapper;

import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;
/**
 *
 * @author ASUS
 */

public class StudentMapper {
    //map Student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student){
        //Membuat dto dengan bulder pattern(inject dari lombok)
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .gradePoint(student.getGradePoint())
                .birthDate (student.getBirthDate() )
                .createdOn (student.getCreatedOn() )
                .updatedOn(student.getUpdatedOn() )
                .build();
        return studentDto;
    }
    //map Student Dto ke Student Entity
    public static Student mapToStudent (StudentDto studentDto) {
        Student student = Student.builder()
            .id (studentDto.getId())
            .firstName (studentDto. getFirstName() )
            .lastName (studentDto.getLastName() )
            .gradePoint(studentDto.getGradePoint())
            .birthDate (studentDto.getBirthDate())
            .createdOn (studentDto.getCreatedOn() )
            .updatedOn (studentDto.getUpdatedOn())
            .build();
        return student;
    }
}
