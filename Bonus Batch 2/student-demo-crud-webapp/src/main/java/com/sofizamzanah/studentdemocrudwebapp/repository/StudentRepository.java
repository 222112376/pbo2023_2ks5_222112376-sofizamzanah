package com.sofizamzanah.studentdemocrudwebapp.repository;
import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
/**
 *
 * @author ASUS
 */

public interface StudentRepository extends JpaRepository<Student, Long> {
    // contoh method abstract baru
    Optional<Student> findByLastName (String lastName);
    Page<Student> findByFirstNameContainingIgnoreCase(String keyword, Pageable pageable);
    Page<Student> findByLastNameContainingIgnoreCase(String keyword, Pageable pageable);
    Page<Student> findByGradePointLessThan(Integer value, Pageable pageable);
    Page<Student> findByFirstNameIsAndLastNameIs(String firstName, String lastName, Pageable pageable);
    Page<Student> findByLastNameStartingWith(String keyword, Pageable pageable);
    Page<Student> findByBirthDateAfterAndBirthDateBefore(Date startDate, Date endDate, Pageable pageable);
}
//Page<UKM> findByNamaUKMContainingIgnoreCase(String keyword, Pageable pageable);
    //Page<UKM> findByKeteranganContainingIgnoreCase(String keyword, Pageable pageable);
/*
* Optional adalah sebuah fitur baru dari Java yang memungkinkan kita untuk menghindari NullPointerException karena Optional bisa berisi sebuah nilai
* atau tidak sama sekali. Jadi jika data mahasiswa dengan nama belakang yang dicari tidak ditemukan, maka method ini akan mengembalikan Optional.empty()
*/