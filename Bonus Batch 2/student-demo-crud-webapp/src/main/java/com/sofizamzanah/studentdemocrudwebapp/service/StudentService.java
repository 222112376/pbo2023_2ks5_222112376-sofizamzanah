package com.sofizamzanah.studentdemocrudwebapp.service;

import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ASUS
 */

public interface StudentService {
    
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);

    public StudentDto cariById(Long id);
    
    public Page<Student> findByFirstNameContainingIgnoreCase(String keyword, Pageable pageable);
    public Page<Student> findAll(Pageable pageable);
    public Page<Student> findByLastNameContainingIgnoreCase(String keyword, Pageable pageable);
    public Page<Student> findByGradePointLessThan(Integer value, Pageable pageable);
    public Page<Student> findByFirstNameIsAndLastNameIs(String firstName, String lastName, Pageable pageable);
    public Page<Student> findByBirthDateAfterAndBirthDateBefore(Date startDate, Date endDate, Pageable pageable);
}

