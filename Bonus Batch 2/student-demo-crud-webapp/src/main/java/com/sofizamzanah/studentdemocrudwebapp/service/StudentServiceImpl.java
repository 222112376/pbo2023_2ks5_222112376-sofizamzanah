package com.sofizamzanah.studentdemocrudwebapp.service;

import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;
import com.sofizamzanah.studentdemocrudwebapp.mapper.StudentMapper;
import com.sofizamzanah.studentdemocrudwebapp.repository.StudentRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author ASUS
 */
@Service
public class StudentServiceImpl implements StudentService{
    private StudentRepository studentRepository;
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    @Override
    public List<StudentDto> ambilDaftarStudent(){
        List<Student> students = this.studentRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi MAP pada Array
        List<StudentDto> studentDtos = students.stream()
                .map(student -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }
    @Override
    public void hapusDataStudent(Long studentId){
        this.studentRepository.deleteById(studentId);
    }

    @Override
    public void perbaruiDataStudent( StudentDto studentDto ){
        Student student = StudentMapper.mapToStudent(studentDto);
        studentRepository.save(student);
    }
    @Override
    public void simpanDataStudent(StudentDto studentDto){
        Student student = StudentMapper.mapToStudent(studentDto);
        studentRepository.save(student);
    }
    @Override
    public StudentDto cariById(Long id){
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
    // Pagination
    @Override
    public Page<Student> findByFirstNameContainingIgnoreCase(String keyword, Pageable pageable){
        return studentRepository.findByFirstNameContainingIgnoreCase(keyword, pageable);
    }
    @Override
    public Page<Student> findAll(Pageable pageable){
        return studentRepository.findAll(pageable);
    }

    @Override
    public Page<Student> findByLastNameContainingIgnoreCase(String keyword, Pageable pageable){
        return studentRepository.findByLastNameContainingIgnoreCase(keyword, pageable);
    }
    @Override
    public Page<Student> findByGradePointLessThan(Integer value, Pageable pageable){
        return studentRepository.findByGradePointLessThan(value, pageable);
    }
    @Override
    public Page<Student> findByFirstNameIsAndLastNameIs(String firstName, String lastName, Pageable pageable){
        return studentRepository.findByFirstNameIsAndLastNameIs(firstName,lastName,pageable);
    }
    @Override
    public Page<Student> findByBirthDateAfterAndBirthDateBefore(Date startDate, Date endDate, Pageable pageable){
        return studentRepository.findByBirthDateAfterAndBirthDateBefore(startDate, endDate, pageable);
    }
}
