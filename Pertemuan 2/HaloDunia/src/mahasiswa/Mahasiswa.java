/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mahasiswa;

/**
 * Author : 222112376_Sofi Zamzanah
 * Deskripsi Singkat Class : class Mahasiswa yang berisi variabel nim dan nama
 */
public class Mahasiswa {
    int nim;
    String nama;
    
    public static void main(String[] args) {
        Mahasiswa s1 = new Mahasiswa();
        System.out.println(s1.nim);
        System.out.println(s1.nama);
    }
}
