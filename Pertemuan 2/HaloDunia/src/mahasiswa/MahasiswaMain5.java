/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mahasiswa;

/**
 * Author : 222112376_Sofi Zamzanah
 * 
 */
public class MahasiswaMain5 {
    public static void main (String args[]){
        Mahasiswa4 s1 = new Mahasiswa4 (222112376, "Sofi Zamzanah");
        Mahasiswa4 s2 = new Mahasiswa4 (222112090, "Hanun Nabila Aziz");
       
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }
}
