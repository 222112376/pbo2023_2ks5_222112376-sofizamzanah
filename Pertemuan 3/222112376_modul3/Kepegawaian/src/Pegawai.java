/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Kepegawaian;

import java.util.Date;

/**
 * Author : 222112376_Sofi Zamzanah
 * Deskripsi Singkat Class : class Pegawai adalah child class dari orang yang memiliki variabel NIP, namaKantor, unitKerja
 */
public class Pegawai extends Orang{
    private String NIP;
    private String namaKantor;
    private String unitKerja;
    
    public Pegawai(){
    }
    
    public Pegawai (String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai (String nama, String NIP, String namaKantor, String unitKerja){
        super(nama);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai (String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public String getNIP(){
        return NIP;
    }
    
    public void setNIP (String NIP){
        this.NIP = NIP;
    }
    
    public String getNamaKantor(){
        return namaKantor;
    }
    
    public void setNamaKantor (String namaKantor){
        this.namaKantor = namaKantor;
    }
    
    public String getUnitKerja(){
        return unitKerja;
    }
    
    public void setUnitKerja (String unitKerja){
        this.unitKerja = unitKerja;
    }
    
    @Override
    public String getGaji(){
        return "7 juta";
    }
}
