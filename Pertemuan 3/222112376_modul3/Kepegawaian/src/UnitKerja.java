/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Kepegawaian;

import java.util.List;

/**
 * Author : 222112376_Sofi Zamzanah
 * Deskripsi Singkat Class : class unitKerja terdapat varabel nama dan list pegawai
 */
public class UnitKerja {
    private String nama;
    private List<Pegawai> daftarPegawai;
    
    public UnitKerja(String nama, List<Pegawai> pegawais){
        this.nama = nama;
        this.daftarPegawai = pegawais;
    }
    
    public String getNama(){
        return nama;
    }
    
    public List<Pegawai> getDaftarPegawai(){
        return daftarPegawai;
    }
}
