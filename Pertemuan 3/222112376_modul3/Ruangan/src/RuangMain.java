/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ruangan;

import java.util.List;

/**
 * Author : 222112376_Sofi Zamzanah
 * 
 */
public class RuangMain {
    public static void main(String[] args){
        Gedung STIS = new Gedung();
        STIS.addRuang ("Lobi");
        STIS.addRuang("Bagian Umum");
        STIS.addRuang("Kepala Kkantor");
        
        List<Ruang> ruangan = STIS.getDaftarRuang();
        for (Ruang ruang : ruangan){
            System.out.println("Ruang : "+ruang.getNamaRuang());
        }
    }
}
