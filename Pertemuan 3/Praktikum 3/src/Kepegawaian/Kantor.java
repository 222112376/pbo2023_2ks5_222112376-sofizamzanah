/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Kepegawaian;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author : 222112376_Sofi Zamzanah
 * Deskripsi Singkat Class : class Kantor yang tidak memiliki variabel karena hanya untuk testing
 */
public class Kantor {
    public static void main (String[] args){
        Orang s1 = new Orang();
        s1.setNama ("Sofi Zamzanah");
        s1.setTanggalLahir (new Date (2001, 2, 25));
        
        Orang s2 = new Orang("Vaniya Dewi Wulandari");
        s2.setTanggalLahir(new Date (2002, 12, 22));
        
        System.out.println("Ada orang:");
        System.out.println("1. "+s1.getNama()+" lahir pada "+s1.getTanggalLahir());
        System.out.println("2. "+s2.getNama()+" lahir pada "+s2.getTanggalLahir());
        
        Pegawai s3 = new Pegawai("Aulia Minati Salsabila",new Date(2002, 7,13),"685694380943","STIS","IT");
        System.out.println("3. "+s3.getNama()+
                " lahir pada "+s3.getTanggalLahir());
        System.out.println("   NIP    : "+s3.getNIP());
        System.out.println("   Kantor : "+s3.getNamaKantor());
        System.out.println("   Bagian : "+s3.getUnitKerja());
        System.out.println();
        
        System.out.println("========== GAJI ==========");
        System.out.println("Gaji Orang "+s2.getNama()+" : "+s2.getGaji());
        System.out.println("Gaji Orang "+s3.getNama()+" : "+s3.getGaji());
        
        List<Pegawai> daftarPegawai = new ArrayList<Pegawai>();
        daftarPegawai.add(s3);
        UnitKerja Umum = new UnitKerja ("umum", daftarPegawai);
    }
    
}
