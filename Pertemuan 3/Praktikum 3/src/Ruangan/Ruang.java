/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ruangan;

/**
 * Author : 222112376_Sofi Zamzanah
 * Deskripsi Singkat Class : class Ruang memiliki variabel namaRuang
 */
public class Ruang {
    private String namaRuang;
    
    public Ruang(String namaRuang){
        this.namaRuang = namaRuang;
    }
    
    public String getNamaRuang(){
        return namaRuang;
    }
    
    public void setNamaRuang(String namaRuang){
        this.namaRuang = namaRuang;
    }
}
