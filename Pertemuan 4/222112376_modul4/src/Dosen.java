/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

/**
 * Author : 222112376_Sofi Zamzanah
 * Class Doesen
 */

public interface Dosen {
    public void setNIDN(String NIDN);
    public String getNIDN();
    public void setKelompokKeahlian(String kelompokKeahlian);
    public String getKelompokKeahlian();   
}

