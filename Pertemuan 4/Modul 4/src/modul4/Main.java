/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

/**
 *
 * @author ASUS
 */
public class Main {
    public static void main(String[] args) {
        // Orang o = new Orang(); // error karena tidak bisa inisialisasi objek; karena merupakan abstract class
        Pegawai lutfi = new Pegawai();

        lutfi.setAlamat("Otista 64C");
        System.out.println(lutfi.getAlamat());

        lutfi.setNIDN("12345678");
        lutfi.setKelompokKeahlian("Computer Science");
        System.out.println("Ada dosen Lutfi dengan NIDN "+lutfi.getNIDN()+" kelompok "+lutfi.getKelompokKeahlian());
    }
}
