/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 * Author : 222112376_Sofi Zamzanah
 * Class Pegawai 
 */
public class Pegawai extends Orang implements Dosen{
    private String NIP;
    private String namaKantor;
    private String unitKerja;
    private String alamat;
    
    private String NIDN;
    private String keahlian;
    
    public Pegawai(){
    }
    
    public Pegawai (String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai (String nama, String NIP, String namaKantor, String unitKerja){
        super(nama);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai (String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public String getNIP(){
        return NIP;
    }
    
    public void setNIP (String NIP){
        this.NIP = NIP;
    }
    
    public String getNamaKantor(){
        return namaKantor;
    }
    
    public void setNamaKantor (String namaKantor){
        this.namaKantor = namaKantor;
    }
    
    public String getUnitKerja(){
        return unitKerja;
    }
    
    public void setUnitKerja (String unitKerja){
        this.unitKerja = unitKerja;
    }
    
    @Override
    public String getGaji(){
        return "7 juta";
    }
    
    @Override
    public String getAlamat(){
        return alamat;
    }
    
    @Override
    public void setAlamat(String alamat){
        this.alamat=alamat;
    }
   
    public String getNIDN(){
        return NIDN;
    }
    
    public void setNIDN(String NIDN){
        this.NIDN=NIDN;
    }
    
    public String getKelompokKeahlian(){
        return keahlian;
    }
    
    public void setKelompokKeahlian(String kelompokKeahlian){
        this.keahlian=kelompokKeahlian;
    }
}
