/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset41;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class AnimalTest {
    public static void main(String[] args){
        //testing untuk class animal
        Animal c1 = new Cat("Pussy");
        System.out.println("Suara kucing c1 : "); 
        c1.greets();
        
        //testing untuk class dog
        Dog d1 = new Dog("Doggy");
        Dog d2 = new Dog("Gukguk");
        System.out.println("Suara anjing d1 : ");
        d1.greets();
        System.out.println("Suara anjing d1 ke d2 : ");
        d1.greets(d2);
        
        //testing unntuk class bigdog
        BigDog bd1 = new BigDog("BigBoss");
        BigDog bd2 = new BigDog("BidiGidi");
        System.out.println("Suara anjing besar bd1 : ");
        bd1.greets();
        System.out.println("Suara anjing besar bd1 ke anjing d1 : ");
        bd1.greets(d1);
        System.out.println("Suara anjing besar bd1 ke anjing besar bd2 :");
        bd1.greets(bd2);
        System.out.print("Suara anjing d1 ke anjing besar bd1 : ");
        d1.greets(bd1);
    }
}
