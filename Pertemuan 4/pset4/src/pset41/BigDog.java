/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset41;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class BigDog extends Dog{
    public BigDog(String name){
        super(name);
    }
    
    @Override
    public void greets(){
        System.out.println("Wooow");
    }
    
    @Override
    public void greets(Dog another){
        System.out.println("Woooooow");
    }
    
    public void greets(BigDog another){
        System.out.println("Wooooooooow");
    }
}
