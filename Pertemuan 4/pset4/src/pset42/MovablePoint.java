/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset42;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class MovablePoint implements Movable{
    int x;
    int y;
    int xSpeed;
    int ySpeed;  
    
    public MovablePoint (int x, int y, int xSpeed, int ySpeed){
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }
    
    @Override
    public String toString(){
        return "(" + x + "," + y + "), speed = (" + xSpeed + "," + ySpeed + ")";
    }
    
    @Override
    public void moveUp(){
        y += ySpeed;
    }
    
    @Override
    public void moveDown(){
        y -= ySpeed;
    }
    
    @Override
    public void moveRight(){
        x += xSpeed;
    }
    
    @Override
    public void moveLeft(){
        x -= xSpeed;
    }
}
