/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset42;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class MovableTest {
    public static void main(String[] args){
        //testing movablePoint, moveUp, dan moveRight
        Movable m1 = new MovablePoint(3, 5, 10, 16);
        System.out.println (m1);
        m1.moveUp();
        m1.moveRight();
        System.out.println(m1);
        
        //testing movableCircle,moveDown, dan moveLeft
        Movable m2 = new MovableCircle(15, 19, 5, 4,7);
        System.out.println (m2);
        m2.moveDown();
        m2.moveLeft();
        System.out.println(m2);
    }
}
