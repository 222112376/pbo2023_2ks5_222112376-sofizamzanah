/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset43;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class MovableTest2 {
   public static void main (String[] args){
       Movable m3 = new MovableRectangle(2, 5, 4, 7, 10, 15);
       System.out.println(m3);
       m3.moveUp();
       m3.moveRight();
       System.out.println(m3);
   } 
}
