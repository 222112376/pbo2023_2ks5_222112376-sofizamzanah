/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset44;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class Circle implements GeometricObject{
    protected double radius;
    
    public Circle (double radius){
        this.radius = radius;
    }
    
    @Override
    public String toString(){
        return "Circle [radius = " + radius + "]";
    }
    
    @Override
    public double getPerimeter(){
        return Math.PI*2*radius;
    }
    
    @Override
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
