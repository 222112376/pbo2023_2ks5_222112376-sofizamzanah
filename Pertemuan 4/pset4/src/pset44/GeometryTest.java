/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset44;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class GeometryTest {
    public static void main(String[] args){
        //testing class circle dan interface GeometricObject
        System.out.println("=============== CIRCLE ===============");
        GeometricObject c1 = new Circle(7);
        System.out.println(c1);
        System.out.println("Luas Lingkaran c1     = " + c1.getArea());
        System.out.println("Keliling Lingkaran c1 = " + c1.getPerimeter());
        System.out.println();
        
        //testing resizableCircle
        System.out.println("========== RESIZABLE CIRCLE ==========");
        System.out.println("----------- INITIAL CIRCLE -----------");
        ResizableCircle c2 = new ResizableCircle(14);
        System.out.println(c2);
        System.out.println("Luas lingkaran awal rc1     = " + c2.getArea());
        System.out.println("Keliling lingkaran awal rc1 = " + c2.getPerimeter());
        System.out.println();     
        System.out.println("------------ FINAL CIRCLE ------------");
        c2.resize(115); 
        System.out.println("Lingkaran c2 diperbesar 15%");      
        System.out.println("Luas lingkaran akhir rc1     = " + c2.getArea());
        System.out.println("Keliling lingkaran akhir rc1 = " + c2.getPerimeter());
    }
}
