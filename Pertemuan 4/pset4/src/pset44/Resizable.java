/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset44;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public interface Resizable {
    public void resize(int percent);
}
