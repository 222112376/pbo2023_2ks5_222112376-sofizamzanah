/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pset44;

/**
 *
 * Author : 222112376_Sofi Zamzanah
 */
public class ResizableCircle extends Circle implements Resizable{
    public ResizableCircle(double radius){
        super(radius);
    }
    
    @Override
    public String toString(){
        return "ResizableCircle [" + super.toString() + "]";
    }
    
    @Override
    public void resize(int percent){
        radius = radius*percent/100;
    }
}
