/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul5;

import java.time.LocalDate;


/**
 *
 * @author 222112376_Sofi Zamzanah
 */
public abstract class Orang {
    private String nama;
    private LocalDate tanggalLahir;
    
    public Orang(String nama, LocalDate tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getNama(){
        return nama;
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }
    
    public String getNamaPanggilan(){
        return nama.substring(0,3);
    }
    
    public LocalDate getTanggalLahir(){
        return tanggalLahir;
    }
    
}
