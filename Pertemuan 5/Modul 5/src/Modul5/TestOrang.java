/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul5;

import java.time.LocalDate;

/**
 *
 * @author 222112376_Sofi Zamzanah
 */
public class TestOrang {
   public static void main(String[] args){
       Pegawai p1 = new Pegawai("Sofi Zamzanah", LocalDate.of(2001, 2,25), "222112376", "BPS Pusat", "Kepala BPS");
       System.out.println("============ INFORMASI PEGAWAI ============");
       System.out.println("Nama Pegawai P1 : " + p1.getNama());
       System.out.println("Nama Panggilan  : " + p1.getNamaPanggilan());
       System.out.println("TTL             : " + p1.getTanggalLahir());
       System.out.println("NIP             : " + p1.getNIP());
       System.out.println("Cabang Kantor   : " + p1.getKantor());
       System.out.println("Unit Kerja      : " + p1.getUnitKerja ());
       System.out.println();
       
       Programmer r1 = new Programmer("Faatin Alifa", LocalDate.of(2002,10,9), "222117463", "BPS Sumatra Barat", "Programmer Kantor Provinsi", "Java", "VSCode"); 
       System.out.println("=========== INFORMASI PROGRAMMER ===========");
       System.out.println("Nama Programmer r1 : " + r1.getNama());
       System.out.println("Nama Panggilan     : " + r1.getNamaPanggilan());
       System.out.println("TTL                : " + p1.getTanggalLahir());
       System.out.println("NIP                : " + r1.getNIP());
       System.out.println("TTL                : " + r1.getTanggalLahir());
       System.out.println("Cabang Kantor      : " + r1.getKantor());
       System.out.println("Unit Kerja         : " + p1.getUnitKerja ());
       System.out.println("Pekerjaan Kerja    : " + r1.getPekerjaan());
       System.out.println("Bahasa Pemrograman : " + r1.getBahasa());
       System.out.println("Platform           : " + r1.getPlatform());
       System.out.println();
    } 
}
