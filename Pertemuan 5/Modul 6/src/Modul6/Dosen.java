/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul6;

/**
 *
 * @author ASUS
 */
public interface Dosen {
    String getNIDN();
    void setNIDN(String NIDN);
    String getKeahlian();
    void setKeahlian(String kelompokKeahlian);
    String getPekerjaan(); 
}

