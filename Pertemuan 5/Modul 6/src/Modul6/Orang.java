/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul6;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author ASUS
 */
public abstract class Orang {
    private String nama;
    private Calendar tanggalLahir;
    
    public Orang(String nama, Calendar tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getNama(){
        return nama;
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }
    
    public String getNamaPanggilan(){
        return nama.substring(0,3);
    }
    
    public Calendar getTanggalLahir(){
        return tanggalLahir;
    }
   
}
