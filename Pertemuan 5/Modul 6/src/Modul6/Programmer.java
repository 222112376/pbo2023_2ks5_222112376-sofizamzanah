/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul6;

import java.util.Calendar;

/**
 *
 * @author ASUS
 */
public class Programmer extends Pegawai{
    private String bahasaPemograman;
    private String platform;

    /*public Programmer(String nama, Date tanggalLahir, String NIP) {
        super(nama, tanggalLahir, NIP);
        this.bahasaPemograman = bahasaPemograman;
        this.platform = platform;
    }*/
    
    public Programmer(String nama, Calendar tanggalLahir, String NIP, String namaKantor, String unitKerja, 
        String bahasaPemograman, String platform){
        super(nama, tanggalLahir, NIP, namaKantor, unitKerja);
        this.bahasaPemograman = bahasaPemograman;
        this.platform = platform;
    }
   
    public String getBahasa(){
        return bahasaPemograman;
    }
    
    public void setBahasa(String bahasaPemograman){
        this.bahasaPemograman = bahasaPemograman;
    }
    
    public String getPlatform(){
        return platform;
    }
    
    @Override
    public String getPekerjaan(){
        return "Coding all along day";
    }
    
    public void setPlatform(String platform){
        this.platform = platform;
    }   
}
