/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package GenericException;

/**
 *
 * @author ASUS
 */
public class BoxTest {
    public static void main (String args[]){
        /*Box boxes[] = new Box[5];
        for(int i = 0; i<boxes.length;i++){
            boxes [i] = new Box();
        }
        boxes[0].set(10);
        boxes[1].set("Polstat STIS");
        Integer someInteger = (Integer) boxes[0].get();
        String someString = (String) boxes[1].get();
        System.out.println(someInteger);
        System.out.println(someString);*/
        
        //old non generic style
        Box stringBox = new Box();
        stringBox.set("Polstat STIS");
        String someString = (String) stringBox.get();
        System.out.println(someString);
        
        //new generic style
        Box1<String> box1 = new Box1<>();
        box1.set("Jakarta");
        String string1 = box1.get(); //no casting
        System.out.println(string1);
        
        //new generic style with another type
        Box1<Integer> box2 = new Box1<>();
        box2.set(100);
        int a = box2.get(); //no casting
        System.out.println(a);
        
        // Testing Interface Pair
        OrderedPair <String, Integer> p1 = new OrderedPair<> ("Even", 8);
        OrderedPair <String, String> p2 = new OrderedPair<> ("Hello", "World");
        OrderedPair<String, Box1<Integer>> p = new OrderedPair<>("primes", new Box1<>());
    }
}
