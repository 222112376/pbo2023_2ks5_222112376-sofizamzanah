/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package GenericException;

/**
 *
 * @author ASUS
 */
public class Generic {
    /*public static void main(String[] args){
        OrderedPair<String, Integer> p1=new OrderedPair<>("Even",8);
        OrderedPair<String, String> p2=new OrderedPair<>("hello","world");
        
        //parameterized type
        OrderedPair<String, Box1<Integer>> p=new OrderedPair<>("primes",new Box1 <>());
    }*/
    static <T> void genericDisplay (T element){
        System.out.println(element.getClass().getName()+ " = "+element);
    }
    
    public static void main(String[] args){
        genericDisplay(110);
        genericDisplay("Polstat STIS");
        genericDisplay(11.0);
        genericDisplay(new Box1 <Integer>());
    }
}
