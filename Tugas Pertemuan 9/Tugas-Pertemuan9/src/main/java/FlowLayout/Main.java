/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FlowLayout;

import javax.swing.SwingUtilities; //untuk membuat antarmuka grafis dengan menggunakan Java Swing.

/**
 *
 * @author ASUS
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(()->{ //untuk menjalankannya melalui EDT
            View view = new View();});
    }
}
