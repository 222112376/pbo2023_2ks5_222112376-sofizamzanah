/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FlowLayout;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class View {
    private Presenter presenter = new Presenter();
    private JFrame frame;
    private JButton btReset;
    private JButton btSimpan;
    private JTextField nimField;
    private JTextField namaDepanField; 
    private JTextField namaBelakangField; 
    private JTextField umurField;
    private JComboBox box;
    private JLabel label;
    
    public View() {
        createUI();
    }
    
    //digunakan untuk menginisialisasi frame utama dan menambahkan elemen-elemen GUI ke dalamnya.
    private void createUI(){ 
        frame = new JFrame("FlowLayout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        frame.setSize(400,200);
        
        label = new JLabel("Nim: ");
        nimField = new JTextField(28);
        frame.add(label);
        frame.add(nimField);
        
        label = new JLabel("Nama: ");
        namaDepanField = new JTextField(14);
        namaBelakangField = new JTextField(14);
        frame.add(label);
        frame.add(namaDepanField);
        frame.add( namaBelakangField);
        
        label = new JLabel("umur: ");
        umurField = new JTextField(28);
        frame.add(label);
        frame.add(umurField);
        
        label = new JLabel("asal: ");
        String[] pilihan = {"-","Jakarta","Bogor","Depok","Tangerang","Bekasi"};
        box = new JComboBox(pilihan);
        frame.add(label);
        frame.add(box);
        
        btReset = new JButton("Reset");
        btSimpan = new JButton("Simpan");
        frame.add(btReset);
        frame.add(btSimpan);
        
        frame.setVisible(true);
        
        btReset.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               nimField.setText("");
               namaDepanField.setText("");
               namaBelakangField.setText("");
               umurField.setText("");
               box.setSelectedIndex(0);
            }
        });
        
        btSimpan.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               JOptionPane.showMessageDialog(frame,
                       presenter.save(nimField.getText(),
                               namaDepanField.getText(),
                       namaBelakangField.getText(),
                       umurField.getText(), 
                       (String) box.getSelectedItem()));               
            }
        });
    }    
}
