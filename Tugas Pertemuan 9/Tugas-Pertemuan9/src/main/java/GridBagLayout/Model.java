/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package GridBagLayout;

/**
 *
 * @author ASUS
 */
public class Model {
    private String nim;
    private String namaDepan;
    private String namaBelakang;
    private String umur;
    private String asal;    

    public Model(String nim, String namaDepan, String namaBelakang, String umur, String asal) {
        this.nim = nim;
        this.namaDepan = namaDepan;
        this.namaBelakang = namaBelakang;
        this.umur = umur;
        this.asal = asal;
    }

    public String getNim() {
        return nim;
    }
    
    public String getNamaDepan() {
        return namaDepan;
    }
    
    public String getNamaBelakang() {
        return namaBelakang;
    }
    
    public String getUmur() {
        return umur;
    }
    
    public String getAsal() {
        return asal;
    }
}
