/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package GridBagLayout;

import FlowLayout.View;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author ASUS
 */
public class Presenter {
    private View view;
    private ArrayList<Model> listModel = new ArrayList<>();
    
    public String simpan(String nim, String namaDepan, String namaBelakang, String umur, String asal){
        String hasil = "";
        
        if (nim.length()!=6){
            hasil += "NIM harus memiliki panjang 6 digit \n";
        }
        if (namaDepan.length()+namaBelakang.length()>50){
            hasil += "Panjang maksimal nama depan + nama belakang adalah 50 digit \n";
        }
        if (!Pattern.matches("[0-9]+",umur)){
            hasil += "Umur hanya bisa diisi oleh angka \n";
        }
        if (nim.length()==0||namaDepan.length()==0||namaBelakang.length()==0||
            umur.length()==0||asal=="-"){
            hasil += "Semua isian wajib terisi\n";
        }
        
        if (hasil.length()==0){
            listModel.add(new Model(nim,namaDepan,namaBelakang,umur,asal));
            int i=0;
            hasil += "Daftar NIM dan Nama Mahasiswa yang berhasil disimpan: \n";
            for(Model m:listModel){
                i++;
                hasil += i+") "+m.getNim()+"-"+m.getNamaDepan()+" "+m.getNamaBelakang()+"\n";
            }
            return hasil;
        } else{
            String keterangan = "Terjadi kesalahan input pada :\n";
            return keterangan+hasil;
        }
    }
}
