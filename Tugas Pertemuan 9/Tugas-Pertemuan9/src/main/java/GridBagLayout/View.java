/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package GridBagLayout;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class View {
    private Presenter presenter = new Presenter();
    private JFrame frame;
    private JButton btReset;
    private JButton btSimpan;
    private JTextField nimField;
    private JTextField namaDepanField; 
    private JTextField namaBelakangField; 
    private JTextField umurField;
    private JComboBox box;
    private JLabel label;
    
    public View() {
        createUI();
    }
    
    private void createUI(){
        frame = new JFrame("GridBagLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container pane = frame.getContentPane();
        pane.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        
        label = new JLabel("Nim:"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.ipadx = 100;
        pane.add(label,c);
        
        nimField = new JTextField(); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.ipadx = 20;
        c.insets = new Insets(0,80,0,0);
        pane.add(nimField,c);
        
        label = new JLabel("Nama:"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.ipadx = 100;
        c.insets = new Insets(0,0,0,0);
        pane.add(label,c);
        
        namaDepanField = new JTextField(); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(namaDepanField,c);
        
        namaBelakangField = new JTextField(); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 1;
        c.ipadx = 100;
        pane.add(namaBelakangField,c);
        
        label = new JLabel("Umur:"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        c.ipadx = 100;
        pane.add(label,c);
        
        umurField = new JTextField(); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 2;
        c.ipadx = 1;
        c.insets = new Insets(0,160,0,0);
        pane.add(umurField,c);
        
        label = new JLabel("Asal:"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        c.ipadx = 100;
        c.insets = new Insets(0,0,0,0);
        pane.add(label,c);
        
        String[] pilihan = {"-","Jakarta","Bogor","Depok","Tangerang","Bekasi"};
        box = new JComboBox(pilihan); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 3;
        c.ipadx = 80;
        c.insets = new Insets(0,20,0,0);
        pane.add(box,c);
        
        btReset = new JButton("Reset"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 4;
        c.ipadx = 90;
        c.insets = new Insets(40,30,10,-20);
        pane.add(btReset,c);
        
        btSimpan = new JButton("Simpan"); 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 4;
        c.ipadx = 90;
        c.insets = new Insets(40,20,10,10);
        pane.add(btSimpan,c);
        
        frame.pack();
        frame.setVisible(true);
        
        btReset.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               nimField.setText("");
               namaDepanField.setText("");
               namaBelakangField.setText("");
               umurField.setText("");
               box.setSelectedIndex(0);
            }
        });
        
        btSimpan.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
               JOptionPane.showMessageDialog(frame,
                       presenter.simpan(nimField.getText(),
                       namaDepanField.getText(),
                       namaBelakangField.getText(),
                       umurField.getText(), 
                       (String) box.getSelectedItem()));               
            }
        });
    }    
}
