// Toggle class active
const navbarNav = document.querySelector ('.navbar .navbar-nav');
const hamburger = document.querySelector('#hamburger-menu');

console.log(hamburger);

hamburger.addEventListener('click', function(e){
    e.preventDefault();
    navbarNav.classList.toggle('active');
});

//menghilangkan nav dg klik diluar side bar
document.addEventListener('click', function(e){
    if(!hamburger.contains(e.target) && !navbarNav.contains(e.target)){
        navbarNav.classList.remove('active');
    }
});

