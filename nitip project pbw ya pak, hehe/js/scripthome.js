let previewContainer = document.querySelector('.product-preview');
let previewBox = previewContainer.querySelectorAll('.preview');

document.querySelectorAll('.macam-ukm-container .product-macam-ukm').forEach(productMacamUkm => {
  productMacamUkm.onclick = () => {
    previewContainer.style.display = 'flex';
    let name = productMacamUkm.getAttribute('data-name');
    previewBox.forEach(preview => {
      let target = preview.getAttribute('data-target');
      if (name == target) {
        preview.classList.add('active');
      }
    });
  };
});

previewBox.forEach(close => {
  close.querySelector('.silang').onclick = () => {
    close.classList.remove('active');
    previewContainer.style.display = 'none';
  };
});
